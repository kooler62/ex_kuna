# ExKuna

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `ex_kuna` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {
      :ex_kuna,
      git: "https://gitlab.com/kooler62/ex_kuna.git",
      branch: "master"
    }
  ]
end
```
Pull dependency by `mix deps.get`  

In  `your_phx_project/config/config.exs` add

```elixir
config :ex_kuna,
       public_key: "PUBLIC_KEYPUBLIC_KEYPUBLIC_KEY",
       secret_key: "SECRET_KEYSECRET_KEYSECRET_KEY"
```


Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/ex_kuna](https://hexdocs.pm/ex_kuna).

